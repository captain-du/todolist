const express = require('express')

const app = express()

const path = require('path')

const bodyParser = require('body-parser')

require('./model/connect')

app.use(express.static(path.join(__dirname, 'public')))

app.use(bodyParser.json())
/* app.use(bodyParser.urlencoded({
    extended: false
})) */

const todo = require('./route/todo')
app.use('/todo', todo)
app.use('/', todo)

app.engine('art', require('express-art-template'))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'art')

app.listen(80)
console.log('服务器已连接');