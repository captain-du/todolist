const mongoose = require('mongoose')

mongoose.connect('mongodb://todo:todo@localhost/todo')
    .then(() => {
        console.log('数据库连接成功');
    })
    .catch(err => {
        console.log('数据库连接失败', err);
    })