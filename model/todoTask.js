const mongoose = require('mongoose')

const todoSchema = new mongoose.Schema({
    completed: {
        type: Boolean,
        default: false
    },
    title: {
        type: String,
        required: true
    }
}, {
    versionKey: false
})

const Todotask = mongoose.model('Todo', todoSchema)

/* Todo.create({
    completed: false,
    title: '吃晚饭'
}, (err, doc) => {
    console.log(err);
    console.log(doc);
}) */

module.exports = Todotask